var Token = artifacts.require("./TOXToken.sol");

var tokenContract;

module.exports = function(deployer) {
    var admin = "0x27acf1FAE84B7bf83DdD417dBeD0a12E12F8F8E0"; 
    var totalTokenAmount = 3 * 100000000000 * 1000000000000000000;
    return Token.new(admin, totalTokenAmount).then(function(result) {
        tokenContract = result;
    });
};
